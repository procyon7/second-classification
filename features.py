
# coding: utf-8

# In[66]:


import sys
import json
import time
import os
import numpy as np
import pandas as pd
import math
import random
import csv
from operator import itemgetter
import collections
import iso8601
import datetime


# In[67]:


file = path = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/yoochoose-buys.dat"
df = pd.read_csv(filepath_or_buffer = file, header=None, sep=',') 
lines = df.iloc[:,0:3].values 


# In[68]:


buy_dict = {}

for i in range(lines.shape[0]):
    inp = lines[i][0]
    val = [lines[i][1],lines[i][2]]
    if inp in buy_dict:
        buy_dict[inp].append(val)
    else:
        buy_dict[inp] = [val]


# In[70]:


#alınan itemlar unique olacak şekilde dict'e kondu 
item_dict = {}

for i in range(lines.shape[0]):
    if lines[i][2] not in item_dict:
        item_dict[lines[i][2]] = i


# In[71]:


f1 = "/Users/rumeysabulut/Documents/Bitirme/yoochoose-data" + "/time-sorted-train.csv"
pf = pd.read_csv(filepath_or_buffer = f1, header=None, sep=',') 
lines_cl = pf.iloc[:,0:3].values


# In[72]:


click_dict = {}
for i in range(lines_cl.shape[0]):
    inp = lines_cl[i][0]
    val = [lines_cl[i][1], lines_cl[i][2]]
    if inp in click_dict:
        click_dict[inp].append(val)
    else:
        click_dict[inp] = [val]


# In[112]:


print(len(click_dict))


# In[77]:


rand_num = random.sample(range(0, lines_cl.shape[0]), lines_cl.shape[0])


# In[78]:


rand_dict = {}
for i in range(len(rand_num)):
    rand_dict[rand_num[i]] = i


# In[109]:


items = len(item_dict)      #buys.dat'taki unique item sayısı = 19949


# In[110]:


#train data kullanılarak itemlar belirlendi. Bu yüzden item sayısının da %70'i kullanılsın
tr = round(items * (0.7))
tr_not = round(items * (0.7))


# In[111]:


print(tr)
print(tr_not)


# In[113]:


#session, buy session olacak. tr kadar bought, tr_not kadar not bought item seçilecek
#item alındıysa satırın sonuna 1, alınmadıysa 0 ekle.
#daha önce clicks.dat'tan reduce edilen train-buyornot.csv kullanıldı.
features = np.empty([tr*2, 7], dtype=object)    #en sona bought mu değil mi koy
index = 0
for key in rand_dict:
    line = lines_cl[key].tolist()       #line = [sess, time, item]
    first_clicked = 0
    last_clicked = 0
    click_num = 0
    is_bought = 0
    time_spent = datetime.datetime.now()
    time_spent -= time_spent   #set time to 0 
    if line[0] in buy_dict and (tr > 0 or tr_not > 0):       #eğer session buying session'sa
        bought = buy_dict.get(line[0])
        buy_item = []
        for j in range(len(bought)):
            buy_item.append(bought[j][1])     #session'ın aldığı itemların listesi, zamana göre sıralı

        time_item = click_dict.get(line[0])    #time_item, session'ın tıkladığı item'ları zamana göre sıralı tutuyor

        it_list = []
        time_li = []
        for i in range(len(time_item)):
            time_li.append(time_item[i][0])
            it_list.append(time_item[i][1])

        if line[2] in buy_item and tr > 0: #item, o session'da alındıysa
            is_bought = 1
            tr = tr - 1
            freq = collections.Counter(it_list)      
            click_num = freq[line[2]]      #F3, Number of times the item was clicked in the session.

            if line[2] == it_list[0]:      #F1, it_list'te itemlar zamana göre sıralı olduğundan [0] ilk tıklanan item
                first_clicked = 1

            if line[2] == it_list[len(it_list) - 1]:      #F2, it_list'te itemlar zamana göre sıralı olduğundan son tıklanan item
                last_clicked = 1

            #F4: Time spent on the item during the session.
            seen = {}
            for pos, it in enumerate(it_list):
                inp = it
                val = pos
                if it in seen:
                    seen[it].append(val)
                else:
                    seen[it] = [val]


            pos_list = []
            for ke in seen:
                if ke == line[2]:
                    pos_list = seen[ke]

            #pos_list'te sadece aradığımız item'ın olduğu index'ler tutuluyor
            total_time = datetime.datetime.now()
            total_time -= total_time
            for m in range(len(pos_list)):
                if pos_list[m] != len(time_li) - 1:       #pos, time listesinin son index'i değilse
                    st = iso8601.parse_date(time_li[pos_list[m]])
                    end = iso8601.parse_date(time_li[pos_list[m] + 1])
                    total_time += end - st

            time_spent = total_time

            l = np.array([line[0], line[2]])
            f = np.array([first_clicked, last_clicked, click_num, time_spent, is_bought])
            features[index] = np.append(l, f)
            index += 1 
 
        elif line[2] not in buy_item and tr_not > 0:

            is_bought = 0
            tr_not = tr_not - 1
            freq = collections.Counter(it_list)      
            click_num = freq[line[2]]      #F3, Number of times the item was clicked in the session.

            if line[2] == it_list[0]:      #F1, it_list'te itemlar zamana göre sıralı olduğundan [0] ilk tıklanan item
                first_clicked = 1

            if line[2] == it_list[len(it_list) - 1]:      #F2, it_list'te itemlar zamana göre sıralı olduğundan son tıklanan item
                last_clicked = 1

            #F4: Time spent on the item during the session.
            seen = {}
            for pos, it in enumerate(it_list):
                inp = it
                val = pos
                if it in seen:
                    seen[it].append(val)
                else:
                    seen[it] = [val]

            pos_list = []
            for ke in seen:
                if ke == line[2]:
                    pos_list = seen[ke]

            #pos_list'te sadece aradığımız item'ın olduğu index'ler tutuluyor
            total_time = datetime.datetime.now()
            total_time -= total_time
            for m in range(len(pos_list)):
                if pos_list[m] != len(time_li) - 1:       #pos, time listesinin son index'i değilse
                    st = iso8601.parse_date(time_li[pos_list[m]])
                    end = iso8601.parse_date(time_li[pos_list[m] + 1])
                    total_time += end - st

            time_spent = total_time

            l = np.array([line[0], line[2]])
            f = np.array([first_clicked, last_clicked, click_num, time_spent, is_bought])
            features[index] = np.append(l, f)
            index += 1

    elif tr == 0 and tr_not == 0:
        break


# In[123]:


t = datetime.datetime.now()
t -= t
for i in range(features.shape[0]):
    t += features[i][5]


# In[125]:


avg_spent = t / features.shape[0]


# In[126]:


print(avg_spent)


# In[127]:


zero = datetime.datetime.now()
zero -= zero
for i in range(features.shape[0]):
    if features[i][5] == zero:
        features[i][5] = avg_spent


# In[128]:


with open('features1234.csv', 'w') as out:
    for i in range(features.shape[0]):
        data = features[i].tolist()
        writer = csv.writer(out)
        writer.writerow(data)

